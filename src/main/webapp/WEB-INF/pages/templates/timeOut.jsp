<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<link rel="stylesheet" href="dashboard/lib/css/timeOut.css" />
	<script type="text/javascript"
	src="dashboard/js/timeOut.js"></script>
	
<!-- The Modal -->
<div id="myModal" class="modal">

	<!-- Modal content -->
	<div class="modal-content">
		<span class="close">&times;</span> <h3>Your session will
			expire in <span id="seconds">60</span> seconds, please click on the screen to continue with the same session!
		</h3>
	</div>

</div>