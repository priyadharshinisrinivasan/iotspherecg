<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="layoutContainer" layout="column" ng-cloak layout-wrap>
	<div flex="10">
		<div id="layoutContainer" layout="row" ng-cloak layout-wrap>
			<div flex="100" class="box1">Support</div>
		</div>
	</div>
	<div flex="90">
		<div class="lines" data-text="IoT Managed Services"></div>
		<div class="col-sm-5" style="margin-left: 13%;">
			<font size=3.8px;> Sreenath Sreekumar<br> Solution
				Architect | Internet Of Things| Advisory, Managed Services<br>
				Ernst &amp; Young<br> Carnival Infopark<br>Kochi,682042,
				Kerala, GDS - India<br> Email : sreenath.ss@xe04.ey.com
			</font>

		</div>

		<div class="col-sm-5" style="margin-left: 3.6%;">
			<font size=3.8px;> Supriya V<br> Associate | Cloud and
				Infrastructure | Global Delivery Services<br> Ernst &amp; Young<br>
				Drishya, Kinfra Campus, <br>Trivandrum 695581, Kerala - India<br>
				Email : Supriya.V@xe04.ey.com
			</font>
		</div>
	</div>
</div>