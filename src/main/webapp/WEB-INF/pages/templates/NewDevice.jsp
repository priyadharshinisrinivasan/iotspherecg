<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="layoutContainer" layout="column" ng-cloak layout-wrap>
	<div flex="10">
		<div id="layoutContainer" layout="row" ng-cloak layout-wrap>
			<div flex="100" class="box1">New Device</div>
		</div>
	</div>
	<div flex="90"></div>
</div>
<!-- Header in top div -->
<div id="layoutContainer" layout="row" ng-cloak layout-wrap>
	<div flex="100" class="box1">
		<md-button ng-click="refresh()"> <span
			class="menu-icon fa fa-refresh"></span> Refresh</md-button>
		<md-button ng-click="d_manage()"> <span
			class="menu-icon fa fa-table"></span> Manage</md-button>
		<md-button ng-click="d_export()"> <span
			class="menu-icon fa fa-cart-plus"></span> Export</md-button>
	</div>
</div>
<!-- End Header in top div -->

<br />
<br />
<div class="container-flued">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-xs-12" ng-controller="NewDevCtrl">
			<div id="msg" ng-show="showmsg">
				<lable> <img ng-src="dashboard/img/{{msg_icon}}"
					style="width: 3%; height: 3%" /> {{message}}</lable>
			</div>
			<form ng-submit="submit()" name="newdeviceform"
				class="form-horizontal">
				<h1>New Device Registration</h1>
				<div class="form-group">
					<label for="deviceid" class="control-label col-xs-2">Device
						Id:</label>
					<div class="col-xs-10">
						<input type="text" class="form-control" name="deviceid"
							id="deviceid" ng-model="newdevice.device_id" required
							placeholder="Device Id" /> <span
							ng-show="(newdeviceform.device_id.$dirty || submitted) && newdeviceform.deviceid.$error.required">
							Device Id is required </span>
					</div>
				</div>
				<div class="form-group">
					<label for="gatewayid" class="control-label col-xs-2">Gateway:</label>
					<div class="col-xs-10">
						<img src="dashboard/img/ajax-loading.gif" style="width: 5%;"
							ng-show="loading" /> <select name="gatewayid" id="gatewayid"
							ng-model='newdevice.gateway_id'
							ng-init="gw = gateway.gateways[0]" class="form-control"
							ng-options="o as o for o in gateway.gateways" ng-hide="loading"></select><span
							ng-show="(newdeviceform.gatewayid.$dirty || submitted) && newdeviceform.gatewayid.$error.required">
							Gateway Id is required </span>
					</div>
				</div>
				<div class="form-group">
					<label for="productid" class="control-label col-xs-2">Product
						Id:</label>
					<div class="col-xs-10">
						<input type="text" class="form-control" name="productid"
							id="productid" ng-model="newdevice.tag.productid" required
							placeholder="Product Id" /> <span
							ng-show="(newdeviceform.productid.$dirty || submitted) && newdeviceform.productid.$error.required">
							Product Id is required </span>
					</div>
				</div>
				<div class="form-group">
					<label for="type" class="control-label col-xs-2">Type:</label>
					<div class="col-xs-10">
						<input type="text" class="form-control" name="type" id="type"
							ng-model="newdevice.tag.type" required placeholder="Type" /><span
							ng-show="(newdeviceform.type.$dirty || submitted) && newdeviceform.type.$error.required">
							Type is required </span>
					</div>
				</div>
				<div class="form-group">
					<label for="firmver" class="control-label col-xs-2">Firmware
						Version:</label>
					<div class="col-xs-10">
						<input type="text" class="form-control" name="firmver"
							id="firmver" ng-model="newdevice.tag.firmver" required
							placeholder="Firmware Version" /> <span
							ng-show="(newdeviceform.firmver.$dirty || submitted) && newdeviceform.firmver.$error.required">
							Firmware Version is required </span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-offset-2 col-xs-10">
						<div class="checkbox">
							<label><input type="checkbox" name="astatus"
								ng-model="newdevice.astatus"
								ng-init="newdevice.tag.astatus = false" /> <span
								ng-show="newdevice.tag.astatus">Alarm On</span><span
								ng-hide="newdevice.tag.astatus">Alarm Off</span> <span
								ng-show="(newdeviceform.astatus.$dirty || submitted) && newdeviceform.astatus.$error.required">
									Alarm Status is required </span></label>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-xs-offset-2 col-xs-10">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
