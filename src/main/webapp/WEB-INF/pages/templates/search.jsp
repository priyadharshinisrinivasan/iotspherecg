<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="layoutContainer" layout="column" ng-cloak layout-wrap>
	<div flex="10">
		<div id="layoutContainer" layout="row" ng-cloak layout-wrap>
			<div flex="100" class="box1">Search</div>
		</div>
	</div>
	<div flex="90"></div>
</div>
<!-- Header in top div -->
<!-- <div id="layoutContainer" layout="row" ng-cloak layout-wrap>
	<div flex="100" class="box1">
		<md-button ng-click=showhide()> <span
			class="menu-icon fa fa-bars"></span> Show and Hide Tables</md-button>
		<md-button ng-click="new_chart_dialogbox()"> <span
			class="menu-icon fa fa-line-chart"></span> Add Chart</md-button>
		<md-button ng-click="refresh()"> <span
			class="menu-icon fa fa-refresh"></span> Refresh</md-button>
		<md-button ng-click="d_manage()"> <span
			class="menu-icon fa fa-table"></span> Settings</md-button>
		<md-button> <span class="menu-icon fa fa-file-pdf-o"></span>
							PDF</md-button>
		<md-button ng-click="d_export()"> <span
			class="menu-icon fa fa-cart-plus"></span> Export</md-button>
	</div>
</div> -->
<!-- End Header in top div -->

<br />
<br />
<!-- <div id="layoutContainer" layout="row" ng-cloak layout-wrap
	layout-align="end">
	<table>
		<tr>
			<td>Search:&nbsp;&nbsp;</td>
			<td><input type='textbox' id="searchKey"
				ng-model="searchKey.text" name="searchKey" required
				placeholder="Search Text" /></td>
			<td><button ng-click=search()>Search</button> &nbsp;</td>
		</tr>
		<tr>
			<td>{{searchKey.status}}</td>
		</tr>
	</table>
	<br />

</div> -->
<div class="container-flued">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12">
			<table style="width: 100%;"
				class="table  table-hover data-table sort display">
				<caption>Gateways</caption>
				<thead>
					<tr>
						<th>Sr.No</th>
						<th>Gateways</th>
						<!-- <th>Action</th> -->
					</tr>
				</thead>
				<tbody ng-repeat="g in searchresp.gateways">
					<tr ng-click="go_gateway(g)">
						<td>{{$index+1}}</td>
						<td>{{g}}</td>
						<!-- <td><a ng-click="toggleSidebar()"> <span
								class="menu-icon glyphicon glyphicon-folder-open"></span></a>&nbsp;<span
							class="menu-icon glyphicon glyphicon glyphicon-trash"></span>&nbsp;<span
							class="menu-icon glyphicon glyphicon glyphicon-edit"></span></td> -->
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12">
			<table style="width: 100%;"
				class="table  table-hover data-table sort display">
				<caption>Devices</caption>
				<thead>
					<tr>
						<th>Sr.No</th>
						<th>Devices</th>
						<th>Gateways</th>
						<!-- <th>Action</th> -->
					</tr>
				</thead>
				<tbody ng-repeat="d in searchresp.devices">
					<tr ng-click="go_device(d[0],'','',0)">
						<td>{{$index+1}}</td>
						<td>{{d[0]}}</td>
						<td>{{d[1]}}</td>
						<!-- <td><a ng-click="toggleSidebar()"> <span
								class="menu-icon glyphicon glyphicon-folder-open"></span></a>&nbsp;<span
							class="menu-icon glyphicon glyphicon glyphicon-trash"></span>&nbsp;<span
							class="menu-icon glyphicon glyphicon glyphicon-edit"></span></td> -->
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
