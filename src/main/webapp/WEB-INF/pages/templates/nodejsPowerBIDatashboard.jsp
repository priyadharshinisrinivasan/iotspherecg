<!doctype html>
<html lang="en" ng-app="IoTSphereCG">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>IoTSphere CG | Dashboard</title>
<!-- STYLES -->
<link rel="stylesheet" href="dashboard/lib/css/main.min.css" />
<link rel="stylesheet" href="dashboard/lib/css/angular-material.min.css" />
<link rel="stylesheet" href="dashboard/lib/css/nv.d3.css" />
<link rel="stylesheet"
	href="dashboard/lib/css/ngDialog-theme-default.css">
<link rel="stylesheet" href="dashboard/lib/css/ngDialog.css">
<link rel="stylesheet" href="dashboard/lib/css/bootstrap.min.css">
<link rel="stylesheet" href="dashboard/lib/css/style.css" />
<!-- SCRIPTS -->
<script src="dashboard/lib/js/main.min.js"></script>
<script src="dashboard/lib/js/angular-material.min.js"></script>
<script src="dashboard/lib/js/angular-animate.min.js"></script>
<script src="dashboard/lib/js/angular-aria.min.js"></script>
<script src="dashboard/lib/js/angular-messages.min.js"></script>
<script src="dashboard/lib/js/angular-dynamic-layout.min.js"></script>
<script src="dashboard/lib/js/d3.min.js"></script>
<script src="dashboard/lib/js/nv.d3.min.js"></script>
<script src="dashboard/lib/js/angular-nvd3.min.js"></script>
<script src="dashboard/lib/js/angular-sanitize.js"></script>
<script src="dashboard/lib/js/ngDialog.min.js"></script>
<script src="dashboard/lib/js/angular-timer.min.js"></script>
<script src="dashboard/lib/js/moment.min.js"></script>
<script src="dashboard/lib/js/humanize-duration.js"></script>
<script src="dashboard/lib/js/jquery.min.js"></script>
<script src="dashboard/lib/js/bootstrap.min.js"></script>
<script src="dashboard/lib/js/angular.easypiechart.js"></script>
<script src="dashboard/lib/js/sockjs.min.js"></script>
<script src="dashboard/lib/js/stomp.min.js"></script>
<!-- Custom Scripts -->
<script type="text/javascript"
	src="dashboard/js/master.js?t=${timestamp}"></script>
<script type="text/javascript"
	src="dashboard/js/config.js?t=${timestamp}"></script>
<script type="text/javascript"
	src="dashboard/js/service.js?t=${timestamp}"></script>
<script type="text/javascript"
	src="dashboard/js/directive.js?t=${timestamp}"></script>
	<script type="text/javascript"
	src="dashboard/js/newdevice.js?t=${timestamp}"></script>
	<script type="text/javascript"
	src="dashboard/js/newgateway.js?t=${timestamp}"></script>
	<script type="text/javascript"
	src="dashboard/js/home.js?t=${timestamp}"></script>
	<script type="text/javascript"
	src="dashboard/js/gateway.js?t=${timestamp}"></script>

<script type="text/javascript">

</script>
<%-- <%@include file="templates/popups.jsp"%> --%>
</head>
<body ng-controller="MasterCtrl">
	<div style="background-color:black;" onmouseover="this.bgColor='white'">
	<!-- <md-button href = "#/nodejsPowerBIEstimationDatashboard"> <span class="menu-icon glyphicon glyphicon-dashboard"></span>
			SMART WAREHOUSE REPORT</md-button> -->
			
	<iframe width="1325" height="750" src="http://iotspheresf.eastus.cloudapp.azure.com:8080/zone1/" frameborder="0" allowFullScreen="true" "&output=embed"></iframe>
	<%-- <iframe width="1325" height="750" src="${nodeJsPowerBiDashboardUrl}" frameborder="0" allowFullScreen="true" "&output=embed"></iframe> --%>
	
	</div>
</body>
</html>