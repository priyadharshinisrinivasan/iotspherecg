
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>IoT Cloud Gateway</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<link
	href="<c:url value="/resources//main/html/assets/plugins/bootstrap/css/bootstrap.min.css"/>"
	rel="stylesheet" />
<link
	href="<c:url value="/resources//main/html/assets/plugins/bootstrap/css/bootstrap-responsive.min.css"/>"
	rel="stylesheet" />
<link
	href="<c:url value="/resources//main/html/assets/plugins/font-awesome/css/font-awesome.min.css"/>"
	rel="stylesheet" />
<link
	href="<c:url value="/resources//main/html/assets/css/style-metro.css"/>"
	rel="stylesheet" />
<link href="<c:url value="/resources//main/html/assets/css/style.css"/>"
	rel="stylesheet" />
<link
	href="<c:url value="/resources//main/html/assets/css/style-responsive.css"/>"
	rel="stylesheet" />
<link
	href="<c:url value="/resources//main/html/assets/css/themes/default.css"/>"
	rel="stylesheet" id="style_color" />

<link rel="stylesheet"
	href="<c:url value="/resources/main/html/assets/plugins/select2/select2_metro.css"/>" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link
	href="<c:url value="/resources/main/html/assets/css/pages/home.css"/>"
	rel="stylesheet" />
<!-- END PAGE LEVEL STYLES -->
<script
	src="<c:url value="/resources/main/html/assets/plugins/jquery-1.10.1.min.js" />"></script>
<script>
	$(window).load(function() {
		$(".loader").fadeOut("slow");
	})
</script>
</head>

<style>
.loader {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url('resources/main/html/assets/img/loading.gif') 50% 50%
		no-repeat rgb(249, 249, 249);
}
</style>
<!-- END HEAD -->
<!-- BEGIN BODY -->


<%-- <script
	src="<c:url value="/resources/main/html/assets/plugins/jquery-validation/dist/jquery.validate.min.js" />"></script>
<script
	src="<c:url value="/resources/main/html/assets/scripts/customform-validation.js" />"></script>
 --%>

<body class="login">
<!-- 	<div class="loader"></div> -->
	<!-- BEGIN LOGO -->
	<div class="logo">
		<img src="resources/main/html/images/EY_Logo_Beam_RGB.png"
				style="width: 35%; margin-top: -3%" class="img-responsive">
	</div>
	<!-- END LOGO -->
	<div class="content" style="text-align: center;">
	<label><b style="font-size: 24px;">IoT Cloud Gateway</b></label><br/>
	<a href="login"><label>Login</label></a><br>
	<!-- <a href="dashboard"><label>Dashboard</label></a> -->
	</div>
	<!-- END LOGIN -->

	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<script
		src="<c:url value="/resources/main/html/assets/plugins/jquery-1.10.1.min.js" />"></script>

	<script
		src="<c:url value="/resources/main/html/assets/plugins/jquery-migrate-1.2.1.min.js" />"></script>

	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script
		src="<c:url value="/resources/main/html/assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" />"></script>
	<script
		src="<c:url value="/resources/main/html/assets/plugins/bootstrap/js/bootstrap.min.js" />"></script>
	<script
		src="<c:url value="/resources/main/html/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" />"></script>
	<script
		src="<c:url value="/resources/main/html/assets/plugins/jquery.blockui.min.js" />"></script>
	<script
		src="<c:url value="/resources/main/html/assets/plugins/jquery.cookie.min.js" />"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->

	<script
		src="<c:url value="/resources/main/html/assets/plugins/backstretch/jquery.backstretch.min.js" />"></script>
	<script
		src="<c:url value="/resources/main/html/assets/plugins/select2/select2.min.js" />"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script
		src="<c:url value="/resources/main/html/assets/plugins/jquery-validation/dist/jquery.validate.min.js" />"></script>
	<script
		src="<c:url value="/resources/main/html/assets/scripts/app.js" />"></script>
	<script
		src="<c:url value="/resources/main/html/assets/scripts/home.js" />"></script>


	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
		jQuery(document).ready(function() {
			App.init();
			Login.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->

</body>
<!-- END BODY -->
</html>