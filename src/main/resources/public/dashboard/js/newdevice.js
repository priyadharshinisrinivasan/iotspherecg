function NewDevCtrl(scope, gw, reg, $http) {
	scope.showmsg = false;
	scope.message = "";
	scope.newdevice = {};
	scope.gateway = {
		gateways : [ "---" ]
	}
	scope.loading = true;
	scope.updategateway = function(gt) {
		scope.gateway = gt;
		scope.loading = false;
		scope.$apply();
	};
	gw.getgateway(scope.updategateway);
	scope.newdevice = {};
	scope.devreg = function(e) {
		scope.message = JSON.stringify(e.data);
		message = (e.data.Status).replace(/\\"/g, '"');
		message = JSON.parse(message);
		if(message.registered == true){
			scope.msg_icon = "greentick.png";
			scope.message = "Registered";
		}
		else{
			scope.msg_icon = "failed.png";
			scope.message = "Error While Registering";
		}
		scope.showmsg = true;
		scope.newdevice = {};
	}

	scope.submit = function() {
		scope.submitted = true;
		var fd = new FormData();
		fd.append('device', JSON.stringify(scope.newdevice));
		reg.register(fd, "registerdevice", scope.devreg)

	};
}

angular.module("IoTSphereCG").controller("NewDevCtrl",
		[ "$scope", "getgateway", "registration", "$http", NewDevCtrl ]);