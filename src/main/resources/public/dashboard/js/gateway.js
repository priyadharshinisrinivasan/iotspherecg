function gatewayCtrl(scope, http, id) {
	scope.gatewayid = id;
	scope.table_devices = {};
	scope.gatewaydata = {};
	scope.loading = true;
	scope.gwloading = true;
	scope.getdevices = function(callback) {
		var http = new XMLHttpRequest();
		var url = "getdevices";
		http.open("POST", url, true);
		http.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		http.onreadystatechange = function() {// Call a function when the
			if (http.readyState == 4 && http.status == 200) {
				devices = JSON.parse(http.responseText);
				devices.devices.forEach(function(e) {
					e.Tag = e.Tag.replace(/\\"/g, '"');
					e.Tag = JSON.parse(e.Tag);
				});
				scope.table_devices = devices;
				scope.loading = false;
				scope.$apply();
			}
		}
		http.send('gateway='+scope.gatewayid);
	};
	scope.getdevices();
	scope.getgatewaydata = function() {
		var http = new XMLHttpRequest();
		var url = "getgatewaydata";
		http.open("POST", url, true);
		http.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		http.onreadystatechange = function() {// Call a function when the
			if (http.readyState == 4 && http.status == 200) {
				gateway = JSON.parse(http.responseText);
				gateway.gatewaydata.Tag = gateway.gatewaydata.Tag.replace(
						/\\"/g, '"');
				gateway.gatewaydata.Tag = JSON.parse(gateway.gatewaydata.Tag);
				scope.gatewaydata = gateway;
				scope.gwloading = false;
				scope.$apply();
			}
		}
		http.send('gateway='+scope.gatewayid);
	};
	scope.getgatewaydata();
}

angular.module("IoTSphereCG").controller("gatewayCtrl",
		[ "$scope", "$http", "param", gatewayCtrl ]);