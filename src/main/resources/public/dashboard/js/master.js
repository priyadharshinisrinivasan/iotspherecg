angular.module("IoTSphereCG", [ "ui.bootstrap", "ui.router", "easypiechart",
			"ngCookies", "ngMaterial", "nvd3", "ngSanitize", "ngDialog",
			"timer" ]);
	"use strict";
function MasterCtrl(t, e, s, nd, h,fu, location) {
	// Variable Initialization
	t.stompClient = null;
	t.calibration_old = {};
	t.notification = "";
	t.flag = true;
	t.notstyle = {color:'white'};
	t.searchKey = {"text":"","status":" "};
	t.calibration = {"Device_Id":"","property":"","value":0, "show": false, "showmsg":"Select Property"};
	t.searchresp = {};
	t.connhome = true;
	t.currentDeviceIndex = 0; // starting device is Zero
	t.intervalFunction = []; // stor interval function for graph
	t.intervalID = [];
	t.interval_running = false;
	t.bulkupbut = false;
	t.gateway = {
			gateways : [ "---" ]
		}
	t.startInterval = function() {
		for (i = 0; i < t.intervalID.length; i++) {
			clearInterval(t.intervalID[i]);
		}
		t.intervalID = [];
		for (i = 0; i < t.intervalFunction.length; i++) {
			temp = setInterval(t.intervalFunction[i], 500);
			t.intervalID.push(temp);
		}
	};
	t.did = 0;// device id
	// ###############################################################################################
	// Send data to db
	postAlertToDatabase = function(dev, jsondata) {
		data = "data=" + JSON.stringify({
			DeviceID : dev,
			data : jsondata
		});
		console.log(data);
		/*
		 * h({ method : 'POST', url : 'alertconfig', data : data, // forms user
		 * object headers : { 'Content-Type' :
		 * 'application/x-www-form-urlencoded' } }).success(function(data) { if
		 * (data.errors) { // Showing errors. } else { } });
		 */
	}

	// End send data to db

	// #############################AddNewTab########################################################
	t.devices = [];// deviceservices.data.devices; // device list from database
	// tabss = []// deviceservices.data.tabs; // tab lists from database
	t.devicelist = [];// deviceservices.data.devicelist; // list of devices
						// connected
	// Dynamic Tabs
	t.currentTab = 0;
	var selected = null, previous = null;
	t.selectedIndex = 0;
	addTab = function(value, init) {
		flex = 33;
		if (value.selectChart == 1)
			flex = 100;
		else if (value.selectChart == 2)
			flex = 50;
		graph_temp = {
			chart : {
				type : 'lineChart',
				height : 400,
				margin : {
					top : 20,
					right : 20,
					bottom : 40,
					left : 55
				},
				x : function(d) {
					return d.x;
				},
				y : function(d) {
					return d.y;
				},
				useInteractiveGuideline : true,
				duration : 500,
				yAxis : {
					tickFormat : function(d) {
						return d3.format('.01f')(d);
					}
				}
			}
		};

		// ######################InsideFunctionAddtab##############################
		var tab_graph = [];
		var tab_graph_data = [];
		var tab_graph_alert = [];
		var alert_post_db = {};
		for (i = 0; i < value.selectChart; i++) {
			tab_graph.push(graph_temp);
			data_temp = {
				x : 0,
				y : [ {
					values : [],
					key : value.chartProperty[i],
					color : value.color[i]
				} ]
			};
			tab_graph_data.push(data_temp);
			if (value.hasOwnProperty("alert")) {
				tab_graph_alert.push(value.alert[i]);
				temp_data = {};
				if (value.alert[i].status) {
					temp_data.status = value.alert[i].status;
					if (value.alert[i].threshold.type.min)
						temp_data.min = value.alert[i].threshold.value.min;
					if (value.alert[i].threshold.type.max)
						temp_data.max = value.alert[i].threshold.value.max;
					if (value.alert[i].threshold.type.xct)
						temp_data.xct = value.alert[i].threshold.value.xct;
					if (value.alert[i].ext.status)
						temp_data.ext = value.alert[i].ext.link;
				} else {
					temp_data.status = false;
				}
				alert_post_db[value.chartProperty[i]] = temp_data;
			}
		}
		t.devices[t.currentDeviceIndex].tabs.push({
			title : value.new_Tab_name,
			graph_flex_size : flex,
			no_of_chart : value.selectChart,
			graph : tab_graph,
			graph_data : tab_graph_data,
			graph_alert : tab_graph_alert,
			disabled : false
		});
		if (t.intervalFunction.length == 0) {
			t.intervalFunction
					.push(function() {
						for (i = 0; i < t.devices[t.currentDeviceIndex].tabs.length; i++) {
							for (j = 0; j < t.devices[t.currentDeviceIndex].tabs[i].graph_data.length; j++) {
								t.devices[t.currentDeviceIndex].tabs[i].graph_data[j].y[0].values
										.push({
											x : t.devices[t.currentDeviceIndex].tabs[i].graph_data[j].x++,
											y : t.devices[t.currentDeviceIndex].device_graph_data[t.devices[t.currentDeviceIndex].tabs[i].graph_data[j].y[0].key]
										});
								if (t.devices[t.currentDeviceIndex].tabs[i].graph_data[j].y[0].values.length > 20)
									t.devices[t.currentDeviceIndex].tabs[i].graph_data[j].y[0].values
											.shift();
							}
						}
						t.$apply(); // update all chart
					});
		}
		if (!init) {
			postAlertToDatabase(t.devices[t.currentDeviceIndex].name,
					alert_post_db);
		}
	};
	t.removeTab = function(tab) {
		var index = tabs.indexOf(tab);
		tabs.splice(index, 1);
	};
	t.onTabChanges = function(tabid) {
		t.currentTab = tabid;
	}
	/*
	 * tabss.forEach(function(entry) { addTab(entry, false); });
	 */
	// End Dynamic Tabs
	// ####################################################################################################

	// ###############################################################################################
	// Devices
	t.style = "panel-default,device-menu";
	// t.devices = this.devices;
	var curtab = {
		name : "Device 1",
		pad : "10px"
	}
	if(t.devices.length >= 1){
	t.dev = t.devices[0].name;
	t.devices[0].pad = "15px";
	}
	t.devchange = function(pid,url) {
		t.flag = false;
		if(url === "#/livedata")
			{
				t.connhome = false;
			}
		else{
			t.connhome = true;
		}
		t.dev = this.devices[pid].name;
		for (i = 0; i < this.devices.length; i++) {
			this.devices[i].pad = "0px";
			this.devices[i].padr = "15px";
			this.devices[i].color = "#d3d7cf"
		}
		this.devices[pid].pad = "15px";
		this.devices[pid].padr = "30px";
		this.devices[pid].color = "#FFFFFF";
		t.currentDeviceIndex = pid;
		t.connect();
		t.startWebsocket();
		//t.getData();
	};
	// End Devices

	// ###############################################################################################
	// ShowHideButton
	t.theight = {
		height : '65%'
	};
	t.sh = true;
	t.showhide = function() {
		if (t.sh) {
			t.sh = false;
			t.theight.height = "100%";
		}

		else if (!t.sh) {
			t.theight.height = "65%";
			t.sh = true;
		}

	};

	// ###############################################################################################
	// Side Menu
	var g = 992;
	t.getWidth = function() {
		return window.innerWidth
	}, t.$watch(t.getWidth, function(o, n) {
		o >= g ? angular.isDefined(e.get("toggle")) ? t.toggle = !!e
				.get("toggle") : t.toggle = !0 : t.toggle = !1
	}), t.toggleSidebar = function() {
		t.toggle = !t.toggle, e.put("toggle", t.toggle)
	}, window.onresize = function() {
		t.$apply()
	}
	// End Side Menu

	// ###############################################################################################

	// DialogBox

	t.new_chart_dialogbox = function() {
		nd
				.openConfirm({
					template : 'new_chart',
					className : 'ngdialog-theme-default',
					height : 'auto',
					overflow : 'auto',
					width : '40%',
					preCloseCallback: function(value) {

                        var nestedConfirmDialog = nd.openConfirm({
                            template:
                                    '<p>Are you sure you want to close?</p>' +
                                    '<div class="ngdialog-buttons">' +
                                        '<button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No' +
                                        '<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(1)">Yes' +
                                    '</button></div>',
                            plain: true,
                            className: 'ngdialog-theme-default'
                        });

                        return nestedConfirmDialog;
                    },
					scope : t
				})
				.then(
						function(value) {
							addTab(value, false);
							if (t.devices[t.currentDeviceIndex].tabs.length == 1) {
								t.selectedIndex = 1;
							} else {
								t.selectedIndex = t.devices[t.currentDeviceIndex].tabs.length - 1;
							}
							value["Device_Id"] = t.devices[t.currentDeviceIndex].name;
							console.log('resolved:' + JSON.stringify(value));
						}, function(value) {
							console.log('rejected:' + value);

						});
	};
	t.d_settings = function() {
		t.calibration.Device_Id = t.devices[t.currentDeviceIndex].name;
		nd.openConfirm({
			template : 'd_settings',
			className : 'ngdialog-theme-default',
			height : 'auto',
			overflow : 'auto',
			width : '40%',
			scope : t
		}).then(function(value) {
			var fd = new FormData();
		       fd.append('Device_Id',t.calibration.Device_Id);
		       fd.append('property',t.calibration.property);
		       fd.append('value',t.calibration.value);
		       h.post("save_calibration", fd, {
		          transformRequest: angular.identity,
		          headers: {'Content-Type': undefined}
		       })
		       .success(function(e){
		    	   console.log("here");
		    	   console.log(e);
		       })
		       .error(function(e){
		       });
			console.log('resolved:' + value);
			// Perform the save here
		}, function(value) {
			console.log('rejected:' + value);

		});
	};
	t.d_export = function() {
		nd.openConfirm({
			template : 'd_export',
			className : 'ngdialog-theme-default',
			height : 'auto',
			overflow : 'auto',
			width : '40%',
			scope : t
		}).then(function(value) {
			console.log('resolved:' + value);
			// Perform the save here
		}, function(value) {
			console.log('rejected:' + value);

		});
	};
	t.d_help = function() {
		nd.openConfirm({
			template : 'd_help',
			className : 'ngdialog-theme-default',
			height : '50%',
			overflow : 'auto',
			width : '40%',
			scope : t
		}).then(function(value) {
			console.log('resolved:' + value);
			// Perform the save here
		}, function(value) {
			console.log('rejected:' + value);

		});
	};
	t.d_about = function() {
		nd.openConfirm({
			template : 'd_about',
			className : 'ngdialog-theme-default',
			height : '50%',
			overflow : 'auto',
			width : '40%',
			scope : t
		}).then(function(value) {
			console.log('resolved:' + value);
			// Perform the save here
		}, function(value) {
			console.log('rejected:' + value);

		});
	};
	
	t.d_manage = function() {
		nd.openConfirm({
			template : 'd_manage',
			className : 'ngdialog-theme-default',
			height : '50%',
			overflow : 'auto',
			width : '40%',
			scope : t
		}).then(function(value) {
			console.log('resolved:' + value);
			// Perform the save here
		}, function(value) {
			console.log('rejected:' + value);

		});
	};
	
	t.devicemanagement = function() {
		t.getgateway();
		nd.openConfirm({
			template : 'devicemanagement',
			className : 'ngdialog-theme-default',
			width : '40%',
			scope : t,
		}).then(function(value) {
			dev = document.getElementById('dev_id').value;
			gw = ((document.getElementById('gateway_id').value).toString()).split(":");
			gw = gw[gw.length-1];
			var devJSON = {};
			devJSON["device_id"]=dev;
			devJSON["gateway_id"]=gw;
			console.log("Device "+dev+" Gateway "+gw+" and "+JSON.stringify(devJSON));
			var fd = new FormData();
		       fd.append('device', JSON.stringify(devJSON));
		       h.post("registerdevice", fd, {
		          transformRequest: angular.identity,
		          headers: {'Content-Type': undefined}
		       })
		       .success(function(e){
		    	   console.log(JSON.stringify(e));
		       })
		       .error(function(e){
		       });
			console.log('resolved:' + value);
			// Perform the save here
		}, function(value) {

			console.log('rejected:' + value);

		});

	};
	
	t.getgateway = function() {
		var http = new XMLHttpRequest();
		var url = "getgateways";
		http.open("POST", url, true);
		// Send the proper header information along with the request
		http.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");

		http.onreadystatechange = function() {// Call a function when the
			// state changes.
			if (http.readyState == 4 && http.status == 200) {
				gateway = http.responseText;
				// console.log("getPort " + typeof(gateway));
				gateway = gateway.replace(/\\"/g, '"');
				// console.log("getPort " + gateway);
				t.gateway = JSON.parse(gateway);
				t.$apply();
				// console.log("getPort " + t.gateway);
			}
		}
		http.send('dummy=dummy');
	}
	
	t.gatewaymanagement = function() {

		nd.openConfirm({
			template : 'registergateway',
			className : 'ngdialog-theme-default',
			width : '40%',
			scope : t,
		}).then(function(value) {
			id = document.getElementById('id').value;
			pid= document.getElementById('pid').value;
			type = document.getElementById('type').value;
			model = document.getElementById('model').value;
			var devJSON = {};
			devJSON["gateway_id"]=id;
			devJSON["product_id"]=pid;
			devJSON["type"]=type;
			devJSON["model"]=model;
			var fd = new FormData();
		       fd.append('gateway', JSON.stringify(devJSON));
		       h.post("registergateway", fd, {
		          transformRequest: angular.identity,
		          headers: {'Content-Type': undefined}
		       })
		       .success(function(e){
		    	   console.log(JSON.stringify(e));
		       })
		       .error(function(e){
		       });			console.log('resolved:' + value);
			// Perform the save here
		}, function(value) {

			console.log('rejected:' + value);

		});

	};
	
	t.d_bulkupload = function() {
		t.choice = {c: "dev"};
		nd.openConfirm({
			template : 'd_bulkupload',
			className : 'ngdialog-theme-default',
			overflow : 'auto',
			width : '40%',
			scope : t
		}).then(function(value) {
			t.bulkupbut = true;
			var formData = new FormData();
			var f = document.getElementById('file').files[0];
			if(t.choice.c == "dev"){
				fu.uploadFileToUrl(f, "devicebulkupload",t.callback);		
			}
			else{
				fu.uploadFileToUrl(f, "gatewaybulkupload",t.callback);
			}
			console.log('resolved:' + value +"file"+typeof(f));
		}, function(value) {
			console.log('rejected:' + value);

		});
	};
	t.callback = function(e){
		if(e.status == 200){
			en = e.data;
			if(en.hasOwnProperty("Status")){
				Job_ID = en.Status;
				t.notstyle.color = 'red';
				t.notinterval = setInterval(function(){
					var http = new XMLHttpRequest();
					var url = "getJobStatus";
					http.open("POST", url, true);
					http.setRequestHeader("Content-type",
					"application/x-www-form-urlencoded");
					http.onreadystatechange = function() {
						if (http.readyState == 4 && http.status == 200) {
							resp = http.responseText;
							resp = resp.replace(/\\"/g, '"');
							resp = JSON.parse(resp);
							t.notification = resp;
							t.noticon();
							console.log(JSON.stringify(resp)+"not "+t.notification.length);
							if(resp.Status == "PC" || resp.Status == "VE" || resp.Status == "IE"){
								t.bulkupbut = false;
								clearInterval(t.notinterval);
								console.log("Interval stopped");
							}
							t.$apply();
						}
					}
					http.send("Job_Id="+Job_ID);
				},1000);
			}
		}
		else{
			t.bulkupbut = false;
		}
	}
	t.notcolor = function(){
		t.notstyle.color = 'white';
	}
	t.noticon = function(){
		if(t.notification.Status == "VE"||t.notification.Status == "IE"){
			t.notification.icon = "failed.png";
		}else if(t.notification.Status == "PC"){
			t.notification.icon = "greentick.png";
		}else{
			t.notification.icon = "loading7_light_blue.gif";
		}
	}
	t.notshow = function(){
		if(t.notification == ""){
			return false;
		}
		else{
			return true;
		}
	}
	t.go_gateway = function(i){
        // console.log("function "+i+" called!!!");
        location.path('gateway/'+i);
    }
	t.go_device = function(i, gateway, status){
        // console.log("function "+i+" called!!!");
        if (t.devicelist.indexOf(i) == -1) {
        	var dev_tab_data = {
					devstatus : "Offline",
					alarm : "Off",
					productid : "MS-IoT-01",
					type : "EY-Gateway-1",
					gateway : "EY-Gateway",
					soft_ver : "1.0.0",
					poll : "10 ms",
					health : "Good",
					power : "5V",
					tx_power : "4 dB",
					operatingband : "2.4 Ghz",
					datarate : "105 bps",
					mac : "A0\:32\:2D\:EB\:43"
				};
        	var data_json = {};
			var alert = {"water":0,"gas":0,"temp":0,"motion":0,"smoke":0,"fire":0};
			var alertcolor = {"water":"#535654","gas":"#535654","temp":"#535654","motion":"#535654","smoke":"#535654","fire":"#535654", "update":false};
			dev = {
				name : i,
				pad : "10px",
				padr : "10px",
				color : "#FFFFFF",
				gateway:gateway,
				status:status,
				items : [ {
					name : "Dashboard",
					icon : "tachometer",
					url : "#/dashboard"
				}, {
					name : "Live Data",
					icon : "line-chart",
					url : "#/livedata"
				}, ],
				device_table_data : dev_tab_data,
				device_graph_data : data_json,
				alert: alert,
				alertcolor: alertcolor,
				tabs : []
			};
			t.devicelist.push(i);
			t.devices.push(dev);
			console.log(t.devicelist.indexOf(i));
			t.devchange(t.devicelist.indexOf(i),"#/dashboard");
			location.path('/dashboard');
		} 
        else{
        	t.devchange(t.devicelist.indexOf(i),"#/dashboard");
        	location.path('/dashboard');
        }
    }
	t.search = function()
	{
		var key = t.searchKey.text;
			t.searchKey.status = "Searching...";
		var http = new XMLHttpRequest();
		http.open("POST", "searchkey", true);
		http.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");

		http.onreadystatechange = function() {
			if (http.readyState == 4 && http.status == 200) {
				resp = http.responseText;
				console.log("resp string " + typeof(resp));
				resp = resp.replace(/\\"/g, '"');
				console.log("replace " + resp);
				t.searchresp = JSON.parse(resp);
				t.searchKey.status = "Completed";
				location.path('/search');
				t.$apply();
			}
		}
		http.send('searchkey='+key);
			
	}

	// End Dialogbox
	// ##################################################################################################

	t.range = (function() {
		var cache = {};
		return function(min, max, step) {
			var isCacheUseful = (max - min) > 70;
			var cacheKey;

			if (isCacheUseful) {
				cacheKey = max + ',' + min + ',' + step;

				if (cache[cacheKey]) {
					return cache[cacheKey];
				}
			}

			var _range = [];
			step = step || 1;
			for (var i = min; i <= max; i += step) {
				_range.push(i);
			}

			if (isCacheUseful) {
				cache[cacheKey] = _range;
			}

			return _range;
		};
	})();

	// ##################################################################################################
	// Labal color change
	t.changeColor = function(value) {
		if (value == "Offline")
			return {
				"color" : "Red"
			};
		else
			return {
				"color" : "Green"
			};
	}

	// ####################################################################################
	// Refress
	t.refresh = function() {
		for (i = 0; i < t.devices.length; i++) {
			t.devices[i].device_graph_data = {};
			for (j = 0; j < t.devices[i].tabs.length; j++) {
				for (k = 0; k < t.devices[i].tabs[j].graph_data.length; k++) {
					t.devices[i].tabs[j].graph_data[k].x = 0;
					t.devices[i].tabs[j].graph_data[k].y[0].values = [];
				}
			}

		}

	}
	// Controller Over
	// ##########################################################################################
	
	// Dashboard data
	 t.scatterChart = {
	            chart: {
	                type: 'scatterChart',
	                height: 450,
	                color: d3.scale.category10().range(),
	                scatter: {
	                    onlyCircles: false
	                },
	                showDistX: true,
	                showDistY: true,
	                tooltipContent: function(key) {
	                    return '<h3>' + key + '</h3>';
	                },
	                duration: 350,
	                xAxis: {
	                    axisLabel: 'Date',
	                    tickFormat: function(d){
	                    	return d3.time.format("%d-%m-%Y")(new Date(d));
	                    }
	                },
	                yAxis: {
	                    axisLabel: 'Data Queality',
	                    tickFormat: function(d){
	                        return d3.format('.02f')(d);
	                    },
	                    axisLabelDistance: -5
	                },
	                zoom: {
	                    // NOTE: All attributes below are optional
	                    enabled: false,
	                    scaleExtent: [1, 10],
	                    useFixedDomain: false,
	                    useNiceScale: false,
	                    horizontalOff: false,
	                    verticalOff: false,
	                    unzoomEventType: 'dblclick.zoom'
	                }
	            }
	        };

	        t.scatterChartdata = generateData(2,10);
	        function getRandomInt(min, max) {
	            return Math.floor(Math.random() * (max - min + 1)) + min;
	        }
	        /* Random Data Generator (took from nvd3.org) */
	        function generateData(groups, points) {
	            var data = [],
	                shapes = ['circle'],
	                key = ['Good','Critical','Motion','Fire','Smoke','Temperature',]
	                random = d3.random.normal();
	            var datestart = 1491004800000;
	            for (var i = 0; i < groups; i++) {
	                data.push({
	                    key: key[i],
	                    values: []
	                });
	                for (var j = 0; j < points; j++) {
	                    data[i].values.push({
	                        x: datestart
	                        , y: getRandomInt(0,100)
	                        , size: Math.random()
	                        , shape: shapes[0]
	                    });
	                    datestart= datestart +5574000;
	                }
	            }
	            return data;
	        }
	       
	        t.pieChart = {
	                chart: {
	                    type: 'pieChart',
	                    height: 450,
	                    x: function(d){return d.key;},
	                    y: function(d){return d.y;},
	                    showLabels: true,
	                    duration: 500,
	                    labelThreshold: 0.01,
	                    labelSunbeamLayout: true,
	                    legend: {
	                        margin: {
	                            top: 5,
	                            right: 35,
	                            bottom: 5,
	                            left: 0
	                        }
	                    }
	                }
	            };

	            t.pieChartdata = [
	                {
	                    key: "Uptime",
	                    y: 80
	                },
	                {
	                    key: "Down Time",
	                    y: 16
	                }
	            ];
	        
	        t.multiBarChart = {
	                chart: {
	                    type: 'multiBarChart',
	                    height: 450,
	                    margin : {
	                        top: 20,
	                        right: 20,
	                        bottom: 45,
	                        left: 45
	                    },
	                    clipEdge: true,
	                    duration: 500,
	                    stacked: true,
	                    xAxis: {
	                        axisLabel: 'Time (ms)',
	                        showMaxMin: false,
	                        tickFormat: function(d){
	                            return d3.format(',f')(d);
	                        }
	                    },
	                    yAxis: {
	                        axisLabel: 'Y Axis',
	                        axisLabelDistance: -20,
	                        tickFormat: function(d){
	                            return d3.format(',.1f')(d);
	                        }
	                    }
	                }
	            };

	            t.multiBarChartdata = generateData1();

	            /* Random Data Generator (took from nvd3.org) */
	            function generateData1() {
	                return stream_layers(3,50+Math.random()*50,.1).map(function(data, i) {
	                	var key = ['Sensor Data','CTD','DTC'];
	                    return {
	                        key: key[i],
	                        values: data
	                    };
	                });
	            }

	            /* Inspired by Lee Byron's test data generator. */
	            function stream_layers(n, m, o) {
	                if (arguments.length < 3) o = 0;
	                function bump(a) {
	                    var x = 1 / (.1 + Math.random()),
	                        y = 2 * Math.random() - .5,
	                        z = 10 / (.1 + Math.random());
	                    for (var i = 0; i < m; i++) {
	                        var w = (i / m - y) * z;
	                        a[i] += x * Math.exp(-w * w);
	                    }
	                }
	                return d3.range(n).map(function() {
	                    var a = [], i;
	                    for (i = 0; i < m; i++) a[i] = o + o * Math.random();
	                    for (i = 0; i < 5; i++) bump(a);
	                    return a.map(stream_index);
	                });
	            }

	            /* Another layer generator using gamma distributions. */
	            function stream_waves(n, m) {
	                return d3.range(n).map(function(i) {
	                    return d3.range(m).map(function(j) {
	                        var x = 20 * j / m - i / 3;
	                        return 2 * x * Math.exp(-.5 * x);
	                    }).map(stream_index);
	                });
	            }

	            function stream_index(d, i) {
	                return {x: i, y: Math.max(0, d)};
	            }
// ############
				t.voltPercent = 35;
				t.voltOptions = {
					animate:{
						duration:1000,
						enabled:true
					},
					scaleColor: '#dfe0e0',
					barColor:'#7b241c',
					lineWidth: 8,
					lineCap:'circle',
					size:150
				};
				t.humidPercent = 25;
				t.humidOptions = {
					animate:{
						duration:1000,
						enabled:true
					},
					scaleColor: '#dfe0e0',
					barColor:'#5b2c6f',
					lineWidth: 8,
					lineCap:'circle',
					size:150
				};
				t.tempPercent = 75;
				t.tempOptions = {
					animate:{
						duration:1000,
						enabled:true
					},
					scaleColor: '#dfe0e0',
					barColor:'#FF0000',
					lineWidth: 8,
					lineCap:'circle',
					size:150
				};
				t.flowPercent = 45;
				t.flowOptions = {
					animate:{
						duration:1000,
						enabled:true
					},
					scaleColor: '#dfe0e0',
					barColor:' #212f3c ',
					lineWidth: 8,
					lineCap:'circle',
					size:150
				};
	        // #################################################################
				t.curdev = function(){
						if(t.devices[t.currentDeviceIndex].name)
							return t.devices[t.currentDeviceIndex].name;
						else
							return null;
				}
				t.res = function(data){
					// console.log(data);
					if(data == null){
						return;
					}
					if(!data.hasOwnProperty("Info") && !data.hasOwnProperty("Error")){
						if(data.hasOwnProperty("deviceid")){
							data["DeviceId"] = data["deivceid"];
							delete data["deviceid"]
						}
						if(data.hasOwnProperty("deviceId")){
							data["DeviceId"] = data["deviceId"];
							delete data["deviceId"];
						}
						if(data.hasOwnProperty("deviceID")){
							data["DeviceId"] = data["deivceID"];
							delete data["deviceID"]
						}
					 var dev_tab_data = { 
							devstatus : "Online", 
							alarm : "Off",
							productid : "MS-IoT-01", 
							type : "EY-Gateway-1", 
							gateway : "EY-Gateway", 
							soft_ver : "1.0.0", 
							poll : "10 ms", 
							health : "Good", 
							power : "5V", 
							tx_power : "4 dB", 
							operatingband : "2.4 Ghz", 
							datarate : "105 bps",
							mac : "A0\:32\:2D\:EB\:43"
						}; 
					var data_json = {};
					for ( var key in data) {
						if (key != "DeviceId")
							data_json[key] = data[key];
					}
					var alert =
					 {"water":0,"gas":0,"temp":0,"motion":0,"smoke":0,"fire":0};
					 var alertcolor =
					{"water":"#535654","gas":"#535654","temp":"#535654","motion":"#535654","smoke":"#535654","fire":"#535654",
					 "update":false}; 
					 if (t.devicelist.indexOf(data["DeviceId"]) == -1) {
					 dev = { 
							 name : data["DeviceId"],
							 pad : "10px", 
							 padr : "10px", 
							 color : "#FFFFFF", 
							 items : [ {
					 name : "Dashboard", 
					 icon : "tachometer", 
					 url : "#/dashboard" }, 
					 { 
						 name : "Live Data",
						 icon : "line-chart", 
						 url : "#/livedata" 
							 }, ], 
						 device_table_data :dev_tab_data, 
					 device_graph_data : data_json, 
					 alert: alert, 
					 alertcolor: alertcolor, 
					 tabs : []
					 };
					 t.devicelist.push(data["DeviceId"]);
					 t.devices.push(dev);
					} else {
						var index = t.devicelist.indexOf(data["DeviceId"]);
						t.devices[index].device_graph_data = data_json;
						t.devices[index].device_table_data["devstatus"] = "Online";
						// console.log("Dev index "+index+" Data"+
						// t.devices[index].device_table_data["devstatus"])
					}
					 
					}
					t.startInterval();
				}
				t.getData = function(){
					if(!t.interval_running){
						setInterval(function(){ fu.fetch_dev_data("readmessage?DeviceId=",t.curdev,t.res); }, 1000);
						t.interval_running = true;
					}			
				}
				
				onConnected = function(wscallback) {
					// Subscribe to the Public Channel
					t.stompClient.subscribe('/channel/public',
							onMessageReceived);
					// Tell your username to the server
					t.stompClient.send("/app/chat.addDevice", {}, JSON
							.stringify({
								sender : 'IoTSphere_Dashboard',
								type : 'JOIN',
								content: {}
							}))
				};

				onError = function(error) {
					console
							.log('Could not connect to WebSocket server. Please refresh this page to try again!');
				};
				onMessageReceived = function(payload) {
					var message = JSON.parse(payload.body);
					if(message.sender != "IoTSphere_Dashboard")
					{
						if(!angular.equals(message.content, {}))
							t.res(message.content);
					}
				}
				sendMessage = function(event) {

					if (t.stompClient) {
						var chatMessage = {
							sender : 'IoTSphere_Dashboard',
							content : {},
							type : 'CHAT'
						};

						t.stompClient.send("/app/chat.sendMessage",
								{}, JSON.stringify(chatMessage));
					}
					event.preventDefault();
				}
				
				t.connect = function()
				{
					if(t.stompClient == null){
						t.stompClient = connectws();
					}
					var headers = {
						      login: '',
						      passcode: '',
						      // additional header
						      Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzaHViaGFtLmR3aXZlZGlAeGUwNC5leS5jb20iLCJyb2xlcyI6InVzZXIiLCJpYXQiOjE1MTM5MjEzOTF9.BA0-iqaCYkiV_ZWCO2C3pWv5orTNXlaqnX_7R2Yc8BY'
						    };
					t.stompClient.connect(headers, onConnected, onError);
				}
				t.startWebsocket = function(){
					var http = new XMLHttpRequest();
					var url = "startWebsocket";
					var data = new FormData();
					data.append('dummy', 'dummy');
					http.open("POST", url, true);
					// Send the proper header information along with the request
					http.setRequestHeader("Content-type",
							"application/x-www-form-urlencoded");

					http.onreadystatechange = function() {// Call a function
															// when the
						// state changes.
						if (http.readyState == 4 && http.status == 200) {
							var response = http.responseText;
							console.log(response);
						}
					}
					http.send('dummy=dummy');
				}
				t.propertycallback = function(resp){
						t.calibration_old = JSON.parse(resp);
				}
				t.calpropchange = function(prop){
					t.calibration.show = false;
					t.calibration.showmsg = "Loading..."
					getpropertyvalue(t.devices[t.currentDeviceIndex].name, prop, t.calibshow);
				}
				t.calibshow = function(val)
				{
					t.calibration.value = parseFloat(val);
					t.calibration.show = true;
				}
				
				t.useCase1 = function() {
				    $('<form action="getNodejsDashboard" method="post"></form>').appendTo('body').submit();
				}; 
				
				t.liveDataInitializeData = function(selectedDeviceId)
				{
					if (t.flag) {
						var http = new XMLHttpRequest();
						var url = "getdevicesFromAzure";
						http.open("POST", url, true);
						http.setRequestHeader("Content-type",
								"application/x-www-form-urlencoded");

						http.onreadystatechange = function() {// Call a function when the
							if (http.readyState == 4 && http.status == 200) {
								devices = JSON.parse(http.responseText);
								devices.devices.forEach(function(e) {
									t.go_device(e.deviceId, e.gatewayId, e.deviceStatus);
								});
								t.$apply();
							}
						}

						http.send("dummy=dummy");
					}
				};
				t.displayMoreDevices = function() {
					$("#displayMoreDevicesId").show();
					t.calibration.Device_Id = t.devices[t.currentDeviceIndex].name;
					nd.openConfirm({
						template : 'displayMoreDevices',
						className : 'ngdialog-theme-default',
						height : 'auto',
						overflow : 'auto',
						width : '40%',
						scope : t
					});
				};
}
angular.module("IoTSphereCG").controller(
		"MasterCtrl",
		[ "$scope", "$cookieStore", "$sce", "ngDialog", "$http","fileUpload", "$location", "websock", MasterCtrl ]);

// ##################################################################################
