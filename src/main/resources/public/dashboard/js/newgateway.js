function NewGateCtrl(scope, reg, http) {
	scope.showmsg = false;
	scope.message = "";
	scope.msg_icon = "";
	scope.newgateway = {};
	scope.devreg = function(e) {
		scope.message = JSON.stringify(e.data);
		message = (e.data.Status).replace(/\\"/g, '"');
		message = JSON.parse(message);
		if(message.registered == true){
			scope.msg_icon = "greentick.png";
			scope.message = "Registered";
		}
		else{
			scope.msg_icon = "failed.png";
			scope.message = "Error While Registering";
		}
		scope.showmsg = true;
		scope.newgateway = {};
	}
	scope.submit = function() {
		scope.submitted = true;
		var fd = new FormData();
		fd.append('gateway', JSON.stringify(scope.newgateway));
		reg.register(fd, "registergateway", scope.devreg)

	};
}

angular.module("IoTSphereCG").controller("NewGateCtrl",
		[ "$scope", "registration", "$http", NewGateCtrl ]);