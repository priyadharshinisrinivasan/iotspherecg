function homeCtrl(scope, http) {
	scope.table_gateways = {};
	scope.loading = true;
	scope.getgateway = function(callback) {
		var http = new XMLHttpRequest();
		var url = "getgateways";
		http.open("POST", url, true);
		http.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		http.onreadystatechange = function() {// Call a function when the
			if (http.readyState == 4 && http.status == 200) {
				gateway = JSON.parse(http.responseText);
				console.log(typeof(gateway));
				scope.table_gateways = gateway;
				scope.loading = false;
				scope.$apply();
			}
		}
		http.send('dummy=dummy');
	};
	scope.getgateway();
}

angular.module("IoTSphereCG").controller("homeCtrl",
		[ "$scope", "$http", homeCtrl ]);