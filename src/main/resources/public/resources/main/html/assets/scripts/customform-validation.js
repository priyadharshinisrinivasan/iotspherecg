 $(function() {
	 	 
	 $('#j_password').keypress(function (e) {
		     var code = (e.keyCode ? e.keyCode : e.which);
		     if (code == 13) {
		        e.preventDefault();
		        e.stopPropagation();
		        $('loginForm').submit();
		     }
		  });
	  
	    // Setup form validation on the #register-form element
	    $("#loginForm").validate({	    
	        // Specify the validation rules
	        rules: {
	            j_username: {
                    required: true,
                    email: true
                },
	            j_password: "required"          
	        },
	        
	        // Specify the validation error messages
	        messages: {
	        	j_username: "Please enter valid Email Id",
	        	j_password: "Please enter your Password"
	        },
	        submitHandler: function(form) {
	        	 if($("input[name=j_username]").val()!="")  //custom validations
		        	{
	        		 form.submit();
		        	}
	            
	        }
	       
	    });

	  });
	  