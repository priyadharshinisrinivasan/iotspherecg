var Login = function () {

	var handleLogin = function() {
		$('.loginForm').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	            	username: {
	                    required: true
	                },
	                password: {
	                    required: true
	                },
	                remember: {
	                    required: false
	                }
	            },

	            messages: {
	            	username: {
	                    required: "Username is required!"
	                },
	                password: {
	                    required: "Password is required!"
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	                $('.alert-error', $('.loginForm')).show();
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.control-group').addClass('error'); // set error class to the control group
	            },

	            success: function (label) {
	            	
	                label.closest('.control-group').removeClass('error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	            	$('.loginForm').submitpost();
	            }
	        });

	        $('.loginForm input').keypress(function (e) {
	            if (e.which == 13) {
	            //	$('.loginForm').submit();
	                if ($('.loginForm').validate().form()) {
	                	$('.loginForm').submitpost();
	                }
	               // return false;
	            }
	        });
	        	    
	        
	        (function( $ ){
	        	   $.fn.submitpost = function() {
	        		 //  <c:url value='j_spring_security_check' />
	        		   $.post( 
	        	                  "login",
	        	                   { username: $("input[name=username]").val(), password: $("input[name=password]").val() },
	        	                  function(data) {
	        	                	  console.log("success");
	        	                  }
	        	               );
	        	   }; 
	        	})( jQuery );
	}

	var handleForgetPassword = function () {
		$('.forget-form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                email: {
	                    required: true,
	                    email: true
	                }
	            },

	            messages: {
	                email: {
	                    required: "Email is required."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.control-group').addClass('error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.control-group').removeClass('error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	            	 $('.forget-form').submitreset();
	         
	            }
	        });

	        $('.forget-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.forget-form').validate().form()) {
	                    $('.forget-form').submitreset();
	                }
	                return false;
	            }
	        });
	        
	        (function( $ ){
				 $.fn.submitreset = function() {
					
					 jQuery("span[id='wait_tip']").css('visibility', 'visible');
						   
					 $.ajax({
						    type: 'POST',
						    url: 'reset',
						    crossDomain: true,
						    data: 'email='+$("input[name=email]").val(),				   
						    dataType: 'text',
						    success: function(data){
						    	jQuery("label[id='loginerror2']").text(data);  	 
						    	jQuery("span[id='wait_tip']").css('visibility', 'hidden');
						    },
						    error: function (responseData, textStatus, errorThrown) {
						        alert(errorThrown);
						    }
						});
				   };
      	})( jQuery );

	        jQuery('#forget-password').click(function () {
	            jQuery('.loginForm').hide();
	            jQuery('.forget-form').show();
	        });

	        jQuery('#back-btn').click(function () {
	            jQuery('.loginForm').show();
	            jQuery('.forget-form').hide();
	        });
	       
	}

	var handleRegister = function () {

		function format(state) {
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='resources/main/html/assets/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
        }


		$("#select2_sample4").select2({
		  	placeholder: '<i class="icon-map-marker"></i>&nbsp;Select a Country',
            allowClear: true,
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });


			$('#select2_sample4').change(function () {
                $('.register-form').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });



         $('.register-form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                
	                fullname: {
	                    required: true
	                },
	                deviceid: {
	                    required: true
	                },
	                emailuser: {
	                    required: true,
	                    email: true
	                },
	                address: {
	                    required: true
	                },
	                city: {
	                    required: true
	                },
	                country: {
	                    required: true
	                },

	                register_password: {
	                    required: true
	                },
	                rpassword: {
	                    equalTo: "#register_password"
	                },

	                tnc: {
	                    required: true
	                }
	            },

	            messages: { // custom messages for radio buttons and checkboxes
	                tnc: {
	                    required: "Please accept TNC first."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.control-group').addClass('error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.control-group').removeClass('error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container                  
	                    error.addClass('help-small no-left-padding').insertAfter($('#register_tnc_error'));
	                } else if (element.closest('.input-icon').size() === 1) {
	                    error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.addClass('help-small no-left-padding').insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	            	 $('.register-form').submitregisterpost();
	           // 	$('.register-form').submit();
	            }
	        });

			$('.register-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.register-form').validate().form()) {
	                //	$('.register-form').submit();
	                    $('.register-form').submitregisterpost();
	                }
	                return false;
	            }
	        });
			
			 (function( $ ){
				 $.fn.submitregisterpost = function() {
					 jQuery("span[id='wait_tip2']").css('visibility', 'visible');
					 $.ajax({
						    type: 'POST',
						    url: 'register',
						    crossDomain: true,
						    data: 'deviceid='+$("input[name=deviceid]").val()+'&email='+$("input[name=emailuser]").val()+'&password='+$("input[name=rpassword]").val()+'&fullname='+$("input[name=fullname]").val()+'&address='+$("input[name=address]").val()+'&city='+$("input[name=city]").val()+'&country='+$("input[name=country]").val(),				   
						    dataType: 'text',
						    success: function(data){
						    	jQuery("span[id='wait_tip2']").css('visibility', 'hidden');
						    	jQuery("label[id='loginerror3']").text(data);  	 
						    },
						    error: function (responseData, textStatus, errorThrown) {
						        alert(errorThrown);
						    }
						});
					 

	        		/*   $.post(
	        	                  "http://localhost:8081/JYWeb/register",
	        	                   {
	        	                	email:"sree",password:"sdf",fullname:"fdf",city:"tvm",country:"in",address:"fdf"
	        	                	  //  email: $("input[name=email]").val(), rpassword: $("input[name=rpassword]").val(),fullname: $("input[name=fullname]").val(),address: $("input[name=address]").val(),city: $("input[name=city]").val(),country: $("input[name=country]").val() 
	        	                   	},
	        	                  function(data) {
	        	                	  console.log("success");
	        	                  }
	        	               );*/
	        	   }; 
	        	})( jQuery );
	      

	        jQuery('#register-btn').click(function () {
	            jQuery('.loginForm').hide();
	            jQuery('.register-form').show();
	        });

	        jQuery('#register-back-btn').click(function () {
	            jQuery('.loginForm').show();
	            jQuery('.register-form').hide();
	        });
	}
    
    return {
        //main function to initiate the module
        init: function () {
        	
            handleLogin();
            handleForgetPassword();
            handleRegister();        

            $.backstretch([
		        "resources/main/html/assets/img/bg/1.jpg",
		        "resources/main/html/assets/img/bg/2.jpg",
		        "resources/main/html/assets/img/bg/3.jpg",
		        "resources/main/html/assets/img/bg/4.jpg"
		        ], {
		          fade: 1000,
		          duration: 8000
		    });
	       
        }

    };

}();