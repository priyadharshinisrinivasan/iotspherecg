package com.ey.iot.iotspherecg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EntityScan(basePackages = { "com.ey.iot.iotspherecg.model" })
@ComponentScan({ "com.ey.iot.iotspherecg.encrypt",
	"com.ey.iot.iotspherecg.configuration", "com.ey.iot.iotspherecg.model",
	"com.ey.iot.iotsphereeg.beans", "com.ey.iot.iotspherecg.websocket",
	"com.ey.iot.iotspherecg.dao", "com.ey.iot.iotspherecg.service",
	"com.ey.iot.iotsphereeg", "com.ey.iot.iotsphereeg.utility",
	"com.ey.iot.iotsphereeg.controller" })
public class Application extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}

}
