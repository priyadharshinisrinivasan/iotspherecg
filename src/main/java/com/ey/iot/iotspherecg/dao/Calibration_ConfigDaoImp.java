package com.ey.iot.iotspherecg.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ey.iot.iotspherecg.model.Calibration_Config;

@Transactional
@Repository
public class Calibration_ConfigDaoImp implements Calibration_ConfigDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Calibration_Config> getAll() {
		return ((Session)this.entityManager.unwrap(Session.class)).createCriteria(Calibration_Config.class).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Calibration_Config getByDeviceId(String DeviceId) {
		Criteria criteria = ((Session)this.entityManager.unwrap(Session.class)).createCriteria(Calibration_Config.class);
		criteria.add(Restrictions.eq("Device_Id", DeviceId));
		List<Calibration_Config> l = criteria.list();
		if(l.isEmpty())
			return new Calibration_Config();
		else
		return l.get(0);
	}

	@Override
	public void saveOrUpdate(Object entity) {
		((Session)this.entityManager.unwrap(Session.class)).saveOrUpdate(entity);
	}

	@Override
	public void persist(Object entity) {
		((Session)this.entityManager.unwrap(Session.class)).persist(entity);
	}

	@Override
	public void delete(Object entity) {
		((Session)this.entityManager.unwrap(Session.class)).delete(entity);
	}

}
