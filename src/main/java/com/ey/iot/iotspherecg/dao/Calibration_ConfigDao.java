package com.ey.iot.iotspherecg.dao;

import java.util.List;

import com.ey.iot.iotspherecg.model.Calibration_Config;


public abstract interface Calibration_ConfigDao {
	
	public abstract List<Calibration_Config> getAll();
	public abstract Calibration_Config getByDeviceId(String DeviceId);
	public abstract void saveOrUpdate(Object entity);
	public abstract void persist(Object entity);
	public abstract void delete(Object entity);
}
