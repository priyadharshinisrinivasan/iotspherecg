package com.ey.iot.iotspherecg.websocket;

import java.lang.reflect.Type;
import java.util.concurrent.ExecutionException;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import com.ey.iot.iotspherecg.configuration.AppProperty;
import com.ey.iot.iotspherecg.model.DeviceMessage;


@Component
public class StompClientLocal {

	public StompSession stompSession = null;
	public String websocket_uri;
	public String token = null;
	public boolean setup = false;
	private static  Logger logger = Logger.getLogger(StompClientLocal.class);
	public StompClientLocal() {
		this.websocket_uri = (new AppProperty()).getPropertyValue("websocket_uri");
		this.token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJtaWMuc2VydkBleS5jb20iLCJyb2xlcyI6InVzZX"
				+ "IiLCJpYXQiOjE1MTY2MjY2Njd9.aLufagzdFM6QOhhvmahHM1aiL4HvIstyetJ9YPNyftI";
	}

	private void connect() {
		WebSocketClient client = new StandardWebSocketClient();
		WebSocketStompClient stompClient = new WebSocketStompClient(client);
		stompClient.setMessageConverter(new MappingJackson2MessageConverter());
		StompSessionHandler sessionHandler = new MyStompSessionHandlerLocal();
		StompHeaders headers = new StompHeaders();
		headers.add("Authorization", "Bearer " + this.token);
		logger.info("Before stompsession fetch!");
		ListenableFuture<StompSession> f = stompClient.connect(this.websocket_uri, new WebSocketHttpHeaders(), headers,
				sessionHandler);
		try {
			this.stompSession = f.get();
			logger.info("Success stompsession CG fetch!");
		} catch (InterruptedException | ExecutionException e) {
			logger.info("Error in stompsession CG fetch!"+e);
			e.printStackTrace();
		}
	}

	public void setup() {
		boolean retry;
		do {
		if (!this.setup) {
			this.connect();
			retry = false;
			}else {
				if(!this.stompSession.isConnected())
				retry = true;
				else
					retry = false;
			}
		} while (retry);
	}

	public synchronized void  sendDeviceData(StompSession stompSession, Object string) {
		//String jsonHello = "{ \"sender\" : \"MicroService\",\"type\" : \"CHAT\",\"content\" : " + string + "}";
		stompSession.send("/app/chat.sendMessage", string);
	}
	
	public class MyStompSessionHandlerLocal extends StompSessionHandlerAdapter {

	    private Logger logger = Logger.getLogger(this.getClass());

	    @Override
	    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
	        logger.info("New session established : " + session.getSessionId());
	        session.subscribe("/channel/public", this);
	        logger.info("Subscribed to /channel/public");
	        session.send("/app/chat.addDevice", getGreetingMessage());
	        logger.info("Message sent to websocket server");
	    }

	    @Override
	    public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
	        logger.error("Got an exception", exception);
	    }

	    @Override
	    public Type getPayloadType(StompHeaders headers) {
	        return DeviceMessage.class;
	    }

	    @Override
	    public void handleFrame(StompHeaders headers, Object payload) {
	    	//DeviceMessage msg = (DeviceMessage) payload;
	       // logger.info("Received Local : " + msg.getContent().toJSONString());
	    }

	    private DeviceMessage getGreetingMessage() {
	    	DeviceMessage msg = new DeviceMessage();
	        msg.setSender("Stomp Client Local");
	        msg.setType(DeviceMessage.MessageType.JOIN);
	        msg.setContent(new JSONObject());
	        return msg;
	    }
	}

}
