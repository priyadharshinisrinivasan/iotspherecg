package com.ey.iot.iotspherecg.configuration;

import java.util.Map;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;

public class HttpHandshakeInterceptor implements HandshakeInterceptor {
	private static Logger logger = Logger.getLogger(HttpHandshakeInterceptor.class);
	public boolean tokencheck(String authHeader) {
		if (authHeader == null || !authHeader.startsWith("Bearer ")) {
			return false;
			// throw new ServletException("Missing or invalid Authorization header");
		}

		final String token = authHeader.substring(7);

		try {
			@SuppressWarnings("unused")
			final Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();
			//request.setAttribute("claims", claims);
			return true;
		} catch (final SignatureException e) {
			return false;
			// throw new ServletException("Invalid token");
		}

	}
	
	@Override
	public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler,
			Exception ex) {

	}

	@Override
	public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler,
			Map<String, Object> attributes) throws Exception {
		if (request instanceof ServletServerHttpRequest) {
			ServletServerHttpRequest servletRequest = (ServletServerHttpRequest) request;
			HttpSession session = servletRequest.getServletRequest().getSession();
			attributes.put("sessionId", session.getId());
			final HttpHeaders Header = request.getHeaders();
			final String authHeader = Header.getFirst("authorization");
			if(tokencheck(authHeader)){
				logger.info("Authetication Done.");
				return true;
			}
			else {
				logger.info("Authentication Problem with websocket connectivity.");
				return false;
			}
				
		} else {
			logger.info("Problem with header, Request dropped");
			return false;
		}
	}

}
