package com.ey.iot.iotspherecg.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@DynamicUpdate(true)
@SelectBeforeUpdate
@Table(name = "ALERTCONFIG")
public class AlertModel implements Serializable {

	/**
	 * AlertModel
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@NotNull
	@Column(name = "Alert_Id", unique = true)
	private int Alert_Id;
	@Column(name = "R_Cre_Time", nullable = false)
	private String rcretime;
	@NotEmpty
	@Column(name = "Deviceid", nullable = false)
	private String Deviceid;
	@NotEmpty
	@Column(name = "Property_id", nullable = true)
	private String property_id;
	@Column(name = "Max_val", nullable = true)
	private String Max_val;
	@Column(name = "min_val", nullable = true)
	private String Min_Val;
	@Column(name = "exact_val", nullable = true)
	private String exact_val;
	@Column(name = "Target_URL", nullable = true)
	private String target_url;
	@Column(name = "Freefld1", nullable = true)
	private String Freefld1;
	@Column(name = "Freefld2", nullable = true)
	private String Freefld2;
	@Column(name = "Freefld3", nullable = true)
	private String Freefld3;
	@Column(name = "R_Mod_Time", nullable = true)
	private String RModTime;
	@Column(name = "deleted", nullable = false)
	private boolean deleted;

	public String getRcretime() {
		return this.rcretime;
	}

	public void setRcretime(String rcretime) {
		this.rcretime = rcretime;
	}

	public String getFreefld1() {
		return this.Freefld1;
	}

	public void setFreefld1(String freefld1) {
		this.Freefld1 = freefld1;
	}

	public String getFreefld2() {
		return this.Freefld2;
	}

	public void setFreefld2(String freefld2) {
		this.Freefld2 = freefld2;
	}

	public String getFreefld3() {
		return this.Freefld3;
	}

	public void setFreefld3(String freefld3) {
		this.Freefld3 = freefld3;
	}

	public String getRModTime() {
		return this.RModTime;
	}

	public void setRModTime(String RModTime) {
		this.RModTime = RModTime;
	}

	public String getDeviceid() {
		return this.Deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.Deviceid = deviceid;
	}

	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AlertModel)) {
			return false;
		}
		AlertModel other = (AlertModel) obj;
		if (this.getAlert_Id() != other.getAlert_Id()) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Alert_Id=" + Alert_Id + ", rcretime=" + rcretime + ", Deviceid=" + Deviceid
				+ ", property_id=" + property_id + ", Max_val=" + Max_val + ", Min_Val=" + Min_Val + ", exact_val="
				+ exact_val + ", target_url=" + target_url + ", Freefld1=" + Freefld1 + ", Freefld2=" + Freefld2
				+ ", Freefld3=" + Freefld3 + ", RModTime=" + RModTime + ", deleted=" + deleted ;
	}

	public int getAlert_Id() {
		return Alert_Id;
	}

	public void setAlert_Id(int alert_Id) {
		Alert_Id = alert_Id;
	}

	public String getProperty_id() {
		return property_id;
	}

	public void setProperty_id(String property_id) {
		this.property_id = property_id;
	}

	public String getMax_val() {
		return Max_val;
	}

	public void setMax_val(String max_val) {
		Max_val = max_val;
	}

	public String getMin_Val() {
		return Min_Val;
	}

	public void setMin_Val(String min_Val) {
		Min_Val = min_Val;
	}

	public String getExact_val() {
		return exact_val;
	}

	public void setExact_val(String exact_val) {
		this.exact_val = exact_val;
	}

	public String getTarget_url() {
		return target_url;
	}

	public void setTarget_url(String target_url) {
		this.target_url = target_url;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

}
