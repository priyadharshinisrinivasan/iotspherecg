package com.ey.iot.iotspherecg.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@DynamicUpdate(true)
@SelectBeforeUpdate
@Table(name = "DEVICECONFIG")
public class DeviceModel implements Serializable {

	/**
	 * DeviceModel
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "Id", unique = true, nullable = false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int Id;
	@Column(name = "R_Cre_Time", nullable = false)
	private String R_Cre_Time;
	@NotEmpty
	@Column(name = "Device_Id", unique = true, nullable = false)
	private String Device_Id;
	@Column(name = "Login_Id", nullable = true)
	private String Login_Id;
	@Column(name = "Data", nullable = true)
	private String Data;
	@Column(name = "Tag", nullable = true)
	private String Tag;
	@ColumnDefault("'false'")
	@Column(name = "Reg_Flg")
	private boolean Reg_Flg;
	@Column(name = "Gateway_Id", nullable = true)
	private String Gateway_Id;
	@Column(name = "Freefld1", nullable = true)
	private String Freefld1;
	@Column(name = "Freefld2", nullable = true)
	private String Freefld2;
	@Column(name = "Freefld3", nullable = true)
	private String Freefld3;
	@Column(name = "R_Mod_Time", nullable = true)
	private String RModTime;
	@ColumnDefault("'false'")
	@Column(name = "Del_Flg", nullable = false )
	private boolean Del_Flg;

	public String getR_Cre_Time() {
		return this.R_Cre_Time;
	}

	public void setR_Cre_Time(String rcretime) {
		if (rcretime.isEmpty())
			rcretime = new Date().toString();
		this.R_Cre_Time = rcretime;
	}

	public String getFreefld1() {
		return this.Freefld1;
	}

	public void setFreefld1(String freefld1) {
		this.Freefld1 = freefld1;
	}

	public String getFreefld2() {
		return this.Freefld2;
	}

	public void setFreefld2(String freefld2) {
		this.Freefld2 = freefld2;
	}

	public String getFreefld3() {
		return this.Freefld3;
	}

	public void setFreefld3(String freefld3) {
		this.Freefld3 = freefld3;
	}

	public String getR_Mod_Time() {
		return this.RModTime;
	}

	public void setR_Mod_Time(String RModTime) {
		if (R_Cre_Time.isEmpty())
			R_Cre_Time = new Date().toString();
		this.RModTime = RModTime;
	}

	public String getDevice_Id() {
		return this.Device_Id;
	}

	public void setDevice_Id(String deviceid) {
		this.Device_Id = deviceid;
	}

	public String getLogin_Id() {
		return this.Login_Id;
	}

	public void setLogin_Id(String loginid) {
		this.Login_Id = loginid;
	}

	public int getId() {
		return this.Id;
	}

	public void setId(int id) {
		this.Id = id;
	}

	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DeviceModel)) {
			return false;
		}
		DeviceModel other = (DeviceModel) obj;
		if (this.Id != other.Id) {
			return false;
		}
		return true;
	}

	public String getData() {
		return Data;
	}

	public void setData(String data) {
		Data = data;
	}



	@Override
	public String toString() {
		return "Id=" + Id + ", R_Cre_Time=" + R_Cre_Time + ", Device_Id=" + Device_Id + ", Login_Id="
				+ Login_Id + ", Data=" + Data + ", Gateway_Id=" + Gateway_Id + ", Freefld1=" + Freefld1 + ", Freefld2="
				+ Freefld2 + ", Freefld3=" + Freefld3 + ", RModTime=" + RModTime + ", Del_Flg=" + Del_Flg;
	}

	public boolean isDel_Flg() {
		return Del_Flg;
	}

	public void setDel_Flg(boolean deleted) {
		this.Del_Flg = deleted;
	}

	public String getGateway_Id() {
		return Gateway_Id;
	}

	public void setGateway_Id(String gateway_Id) {
		Gateway_Id = gateway_Id;
	}

	public String getTag() {
		return Tag;
	}

	public void setTag(String tag) {
		Tag = tag;
	}

	public boolean isReg_Flg() {
		return Reg_Flg;
	}

	public void setReg_Flg(boolean reg_Flg) {
		Reg_Flg = reg_Flg;
	}
}
