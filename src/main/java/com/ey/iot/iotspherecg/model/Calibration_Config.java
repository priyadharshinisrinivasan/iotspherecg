package com.ey.iot.iotspherecg.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

@Entity
@DynamicUpdate(true)
@SelectBeforeUpdate
@Table(name = "Calibration_Config")
public class Calibration_Config implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "Id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Id;
	@Column(name = "Device_Id", unique = true, nullable = false)
	private String Device_Id;
	@Column(name = "Property", nullable = false)
	private String Property;
	@Column(name = "R_Cre_Time", nullable = false)
	private String R_Cre_Time;
	@Column(name = "R_Mod_Time")
	private String R_Mod_Time;
	@Column(name = "Freefld1")
	private String Freefld1;
	@Column(name = "Freefld2")
	private String Freefld2;
	@Column(name = "Freefld3")
	private String Freefld3;
	@ColumnDefault("'false'")
	@Column(name = "Del_Flg", nullable = false )
	private boolean Del_Flg;
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getDevice_Id() {
		return Device_Id;
	}
	public void setDevice_Id(String device_Id) {
		Device_Id = device_Id;
	}
	public String getProperty() {
		return Property;
	}
	public void setProperty(String property) {
		Property = property;
	}
	public String getR_Cre_Time() {
		return R_Cre_Time;
	}
	public void setR_Cre_Time(String r_Cre_Time) {
		R_Cre_Time = r_Cre_Time;
	}
	public String getR_Mod_Time() {
		return R_Mod_Time;
	}
	public void setR_Mod_Time(String r_Mod_Time) {
		R_Mod_Time = r_Mod_Time;
	}
	public String getFreefld1() {
		return Freefld1;
	}
	public void setFreefld1(String freefld1) {
		Freefld1 = freefld1;
	}
	public String getFreefld2() {
		return Freefld2;
	}
	public void setFreefld2(String freefld2) {
		Freefld2 = freefld2;
	}
	public String getFreefld3() {
		return Freefld3;
	}
	public void setFreefld3(String freefld3) {
		Freefld3 = freefld3;
	}
	@Override
	public String toString() {
		return "Id=" + Id + ", Device_Id=" + Device_Id + ", Property=" + Property + ", R_Cre_Time="
				+ R_Cre_Time + ", R_Mod_Time=" + R_Mod_Time + ", Freefld1=" + Freefld1 + ", Freefld2=" + Freefld2
				+ ", Freefld3=" + Freefld3 + ", Del_Flg=" + Del_Flg;
	}
}
