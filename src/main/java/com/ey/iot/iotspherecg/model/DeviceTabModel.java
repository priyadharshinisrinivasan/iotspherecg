package com.ey.iot.iotspherecg.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@DynamicUpdate(true)
@SelectBeforeUpdate
@Table(name = "DEVICETABCONFIG")
public class DeviceTabModel implements Serializable {

	/**
	 * DeviceTabModel
	 */
	private static final long serialVersionUID = 1L;
	@NotEmpty
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int Id;
	@Column(name = "R_Cre_Time", nullable = false)
	private String rcretime;
	@NotEmpty
	@Column(name = "Deviceid", nullable = false)
	private String Deviceid;
	@Column(name = "TabName", nullable = false)
	private String TabName;
	@Column(name = "no_of_chart", nullable = false)
	private int no_of_chart;
	@Column(name = "disabled", nullable = false )
	@ColumnDefault("'false'")
	private boolean disabled;
	@Column(name = "graph_one", nullable = true)
	private String graph_one;
	@Column(name = "graph_two", nullable = true)
	private String graph_two;
	@Column(name = "graph_three", nullable = true)
	private String graph_three;
	@Column(name = "graph_one_color", nullable = true)
	private String graph_one_color;
	@Column(name = "graph_two_color", nullable = true)
	private String graph_two_color;
	@Column(name = "graph_three_color", nullable = true)
	private String graph_three_color;
	@Column(name = "graph_one_alert", nullable = false )
	@ColumnDefault("'false'")
	private boolean graph_one_alert;
	@Column(name = "graph_two_alert", nullable = false )
	@ColumnDefault("'false'")
	private boolean graph_two_alert;
	@Column(name = "graph_three_alert", nullable = false )
	@ColumnDefault("'false'")
	private boolean graph_three_alert;
	@Column(name = "Freefld1", nullable = true)
	private String Freefld1;
	@Column(name = "Freefld2", nullable = true)
	private String Freefld2;
	@Column(name = "Freefld3", nullable = true)
	private String Freefld3;
	@Column(name = "R_Mod_Time", nullable = true)
	private String RModTime;
	@ColumnDefault("'false'")
	@Column(name = "deleted", nullable = false)
	private boolean deleted;/*
	@Column(name = "graph_one_calibration",  nullable = false)
	private int graph_one_calibration;
	@Column(name = "graph_one_calibration", nullable = false)
	private int graph_two_calibration;
	@Column(name = "graph_one_calibration", nullable = false)
	private int graph_three_calibration;
*/
	public String getRcretime() {
		return this.rcretime;
	}

	public void setRcretime(String rcretime) {
		this.rcretime = rcretime;
	}

	public String getFreefld1() {
		return this.Freefld1;
	}

	public void setFreefld1(String freefld1) {
		this.Freefld1 = freefld1;
	}

	public String getFreefld2() {
		return this.Freefld2;
	}

	public void setFreefld2(String freefld2) {
		this.Freefld2 = freefld2;
	}

	public String getFreefld3() {
		return this.Freefld3;
	}

	public void setFreefld3(String freefld3) {
		this.Freefld3 = freefld3;
	}

	public String getRModTime() {
		return this.RModTime;
	}

	public void setRModTime(String RModTime) {
		this.RModTime = RModTime;
	}

	public String getDeviceid() {
		return this.Deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.Deviceid = deviceid;
	}

	public int getId() {
		return this.Id;
	}

	public void setId(int id) {
		this.Id = id;
	}

	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DeviceTabModel)) {
			return false;
		}
		DeviceTabModel other = (DeviceTabModel) obj;
		if (this.Id != other.Id) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "JYModel [id=" + this.Id + ", Deviceid=" + this.Deviceid + ", Freefld1="
				+ this.Freefld1 + ", Freefld2=" + this.Freefld2 + ", Freefld3=" + this.Freefld3 + ", R_Cre_Time="
				+ this.rcretime + ", R_Mod_Time=" + this.RModTime + "]";
	}

	public String getTabName() {
		return TabName;
	}

	public void setTabName(String tabName) {
		TabName = tabName;
	}

	public int getNo_of_chart() {
		return no_of_chart;
	}

	public void setNo_of_chart(int no_of_chart) {
		this.no_of_chart = no_of_chart;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public String getGraph_one() {
		return graph_one;
	}

	public void setGraph_one(String graph_one) {
		this.graph_one = graph_one;
	}

	public String getGraph_two() {
		return graph_two;
	}

	public void setGraph_two(String graph_two) {
		this.graph_two = graph_two;
	}

	public String getGraph_three() {
		return graph_three;
	}

	public void setGraph_three(String graph_three) {
		this.graph_three = graph_three;
	}

	public String getGraph_one_color() {
		return graph_one_color;
	}

	public void setGraph_one_color(String graph_one_color) {
		this.graph_one_color = graph_one_color;
	}

	public String getGraph_two_color() {
		return graph_two_color;
	}

	public void setGraph_two_color(String graph_two_color) {
		this.graph_two_color = graph_two_color;
	}

	public String getGraph_three_color() {
		return graph_three_color;
	}

	public void setGraph_three_color(String graph_three_color) {
		this.graph_three_color = graph_three_color;
	}

	public boolean isGraph_one_alert() {
		return graph_one_alert;
	}

	public void setGraph_one_alert(boolean graph_one_alert) {
		this.graph_one_alert = graph_one_alert;
	}

	public boolean isGraph_two_alert() {
		return graph_two_alert;
	}

	public void setGraph_two_alert(boolean graph_two_alert) {
		this.graph_two_alert = graph_two_alert;
	}

	public boolean isGraph_three_alert() {
		return graph_three_alert;
	}

	public void setGraph_three_alert(boolean graph_three_alert) {
		this.graph_three_alert = graph_three_alert;
	}
}
