package com.ey.iot.iotspherecg.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@Entity
@EnableAutoConfiguration
@Table(name = "GATEWAYCONFIG")
public class GatewayModel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "Id", unique = true, nullable = false)	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Id;
	@Column(name = "Gateway_Id", unique = true, nullable = false)
	private String Gateway_Id;
	@Column(name = "Tag", nullable = true)
	private String Tag;
	@Column(name = "Reg_Flg")
	private boolean Reg_Flg;
	@Column(name = "R_Cre_Time", nullable = false)
	private String R_Cre_Time;
	@Column(name = "Freefld1", nullable = true)
	private String Freefld1;
	@Column(name = "Freefld2", nullable = true)
	private String Freefld2;
	@Column(name = "Freefld3", nullable = true)
	private String Freefld3;
	@Column(name = "R_Mod_Time", nullable = true)
	private String R_Mod_Time;
	@ColumnDefault("'false'")
	@Column(name = "Del_Flg", nullable = false)
	private boolean Del_Flg;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getGateway_Id() {
		return Gateway_Id;
	}
	public void setGateway_Id(String gateway_Id) {
		Gateway_Id = gateway_Id;
	}
	public boolean isReg_Flg() {
		return Reg_Flg;
	}
	public void setReg_Flg(boolean reg_Flg) {
		Reg_Flg = reg_Flg;
	}
	public String getR_Cre_Time() {
		return R_Cre_Time;
	}
	public void setR_Cre_Time(String r_Cre_Time) {
		R_Cre_Time = r_Cre_Time;
	}
	public String getFreefld1() {
		return Freefld1;
	}
	public void setFreefld1(String freefld1) {
		Freefld1 = freefld1;
	}
	public String getFreefld2() {
		return Freefld2;
	}
	public void setFreefld2(String freefld2) {
		Freefld2 = freefld2;
	}
	public String getFreefld3() {
		return Freefld3;
	}
	public void setFreefld3(String freefld3) {
		Freefld3 = freefld3;
	}
	public String getR_Mod_Time() {
		return R_Mod_Time;
	}
	public void setR_Mod_Time(String r_Mod_Time) {
		R_Mod_Time = r_Mod_Time;
	}
	public boolean isDel_Flg() {
		return Del_Flg;
	}
	public void setDel_Flg(boolean del_Flg) {
		Del_Flg = del_Flg;
	}
	public String getTag() {
		return Tag;
	}
	public void setTag(String tag) {
		Tag = tag;
	}
}
