package com.ey.iot.iotspherecg.controller;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import javax.websocket.Session;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.ey.iot.iotspherecg.encrypt.BCryptutil;
import com.ey.iot.iotspherecg.model.LoginModel;
import com.ey.iot.iotspherecg.service.EYService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

@SuppressWarnings("unused")
@RestController
@RequestMapping("/jwt")
public class JWTSecureController extends AppController {

	@Autowired
	EYService eyservice;
	public Session session;

	@SuppressWarnings("null")
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public @ResponseBody String postauthenticate(final ModelMap model, HttpServletRequest request,
			UriComponentsBuilder ucBuilder) throws IOException {
		final String authHeader = request.getHeader("authorization");
		String emailuser = null;
		String password = null;
		try {
			if (authHeader != null || authHeader.startsWith("Basic ")) {

				String credentials = authHeader.substring(6);
				Base64 base64 = new Base64();
				String decoded = new String(base64.decode(credentials.getBytes()));
				String parts[] = decoded.split(":");
				emailuser = parts[0];
				password = parts[1];
			} else {
				return "failure";
			}

			List<LoginModel> jymodel = this.eyservice.finddata(emailuser);
			String computedhash = jymodel.get(0).getPwd();
			boolean flg = BCryptutil.verifypwd(password, computedhash);
			//String msg;
			if (flg == true) {
				String jwtToken = "";
				jwtToken = Jwts.builder().setSubject(emailuser).claim("roles", "user").setIssuedAt(new Date())
						.signWith(SignatureAlgorithm.HS256, "secretkey").compact();
				return jwtToken;
			} else {
				return "failure";
			}
		} catch (Exception e) {
			return "failure";
		}

	}

	public boolean tokencheck(HttpServletRequest request) {
		final String authHeader = request.getHeader("authorization");
		if (authHeader == null || !authHeader.startsWith("Bearer ")) {
			return false;
			// throw new ServletException("Missing or invalid Authorization header");
		}

		final String token = authHeader.substring(7);

		try {
			final Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();
			request.setAttribute("claims", claims);
			return true;
		} catch (final SignatureException e) {
			return false;
			// throw new ServletException("Invalid token");
		}

	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public @ResponseBody String postInsert(final ModelMap model, @Valid LoginModel jymodel, BindingResult result,
			HttpServletRequest request, @RequestParam("data") String data, UriComponentsBuilder ucBuilder)
			throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		boolean token_validation = tokencheck(request);
		if (token_validation) {
			return "Success";
		} else
			return "Invalid Token";
	}
	
	@RequestMapping(value = { "/post" }, method = { RequestMethod.POST })
	@ResponseBody
	public String postMethod(ModelMap model, HttpServletRequest request, @RequestParam("data") String data,
			UriComponentsBuilder ucBuilder) throws IOException {
		model.addAttribute("postdata", data);
		if ((this.session == null) || (!this.session.isOpen())) {
			System.out.println("Reconnect..");
			start();
		}
		System.out.println("Session" + this.session.getId());
		send(data);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/data").buildAndExpand(new Object[] { "/data" }).toUri());
		return data;
	}

	public void send(String JSONstring) {
		if ((this.session == null) || (!this.session.isOpen())) {
			System.out.println("Reconnect..");
			start();
		}
		try {
			this.session.getBasicRemote().sendText(JSONstring);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void start() {
		/*
		 * WebSocketContainer container = ContainerProvider.getWebSocketContainer(); try
		 * { System.out.println(this.websocket_uri); this.session =
		 * container.connectToServer(MyClient.class, URI.create(this.websocket_uri)); }
		 * catch (DeploymentException e) { e.printStackTrace(); } catch (IOException e)
		 * { e.printStackTrace(); }
		 */
	}

}
