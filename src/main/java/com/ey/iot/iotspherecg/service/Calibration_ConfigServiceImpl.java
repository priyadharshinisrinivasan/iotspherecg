/**
 * 
 */
package com.ey.iot.iotspherecg.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ey.iot.iotspherecg.dao.Calibration_ConfigDao;
import com.ey.iot.iotspherecg.model.Calibration_Config;


/**
 * @author Shubham.Dwivedi
 *
 */
@Service
public class Calibration_ConfigServiceImpl implements Calibration_ConfigService {
	
	@Autowired
	Calibration_ConfigDao dao;

	@Override
	public List<Calibration_Config> getAll() {
		return dao.getAll();
	}

	@Override
	public Calibration_Config getByDeviceId(String DeviceId) {
		return dao.getByDeviceId(DeviceId);
	}

	@Override
	public void saveOrUpdate(Calibration_Config entity) {
		dao.saveOrUpdate(entity);
	}

	@Override
	public void persist(Calibration_Config entity) {
		dao.persist(entity);
	}

	@Override
	public void delete(Calibration_Config entity) {
		dao.delete(entity);
	}

}
