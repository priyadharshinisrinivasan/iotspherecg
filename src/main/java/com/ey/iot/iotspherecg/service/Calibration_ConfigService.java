package com.ey.iot.iotspherecg.service;

import java.util.List;

import com.ey.iot.iotspherecg.model.Calibration_Config;


/**
 * @author Shubham.Dwivedi
 *
 */
public abstract interface Calibration_ConfigService {
	
	public abstract List<Calibration_Config> getAll();
	public abstract Calibration_Config getByDeviceId(String DeviceId);
	public abstract void saveOrUpdate(Calibration_Config entity);
	public abstract void persist(Calibration_Config entity);
	public abstract void delete(Calibration_Config entity);
	
}
