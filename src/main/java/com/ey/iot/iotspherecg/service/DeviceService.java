package com.ey.iot.iotspherecg.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.ey.iot.iotspherecg.model.DeviceModel;
import com.google.gson.JsonSyntaxException;
import com.microsoft.azure.sdk.iot.service.Device;
import com.microsoft.azure.sdk.iot.service.exceptions.IotHubException;


public abstract interface DeviceService {
	public abstract void savedata(DeviceModel paramDeviceModel);

	public abstract void deletedata(String paramString);

	public abstract List<DeviceModel> finddata(String paramString);

	public abstract List<DeviceModel> findall();

	public abstract List<DeviceModel> searchDevices(String keyword);

	public abstract List<DeviceModel> getDeviceByGatewayId(String gateway);

	public abstract boolean addDevice(DeviceModel paramDeviceModel);

	public abstract boolean isDeviceExists(String paramString);

	public abstract void saveOrUpdate(DeviceModel device);
	
	public ArrayList<Device> getDevicesFromAzure() throws IOException, JsonSyntaxException, IotHubException;
}
