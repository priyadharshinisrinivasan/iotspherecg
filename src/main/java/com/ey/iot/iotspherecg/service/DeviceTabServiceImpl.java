package com.ey.iot.iotspherecg.service;


import java.util.List;

import org.hibernate.annotations.DynamicUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ey.iot.iotspherecg.dao.DeviceTabDao;
import com.ey.iot.iotspherecg.model.DeviceTabModel;

@Service("DeviceTabService")
@DynamicUpdate(true)
@Transactional
public class DeviceTabServiceImpl
  implements DeviceTabService
{
  @Autowired
  private DeviceTabDao dao;
  
  public void savedata(DeviceTabModel DeviceTabModel)
  {
    this.dao.saveOrUpdate(DeviceTabModel);
  }
  
  public List<DeviceTabModel> finddata(String input)
  {
    return this.dao.finddata(input);
  }
  
  
  public void deletedata(String ssn)
  {
    this.dao.deletedata(ssn);
  }

 
}
