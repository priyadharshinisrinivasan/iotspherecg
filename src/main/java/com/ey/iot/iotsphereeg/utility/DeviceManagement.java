package com.ey.iot.iotsphereeg.utility;

import java.io.IOException;
import java.util.ArrayList;

import javax.annotation.Resource;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.JsonSyntaxException;
import com.microsoft.azure.sdk.iot.service.Device;
import com.microsoft.azure.sdk.iot.service.RegistryManager;
import com.microsoft.azure.sdk.iot.service.exceptions.IotHubException;

@Component
public class DeviceManagement {
	@Resource
	@Value("${connectionStringIoTHub}")
	public String connectionStringIoTHub;
	
	/*public DeviceManagement(String a)
	{
		this.connectionStringIoTHub = (new AppProperty()).getPropertyValue("connectionStringIoTHub");
		//System.out.println("Connection String: "+this.connectionStringIoTHub);
	}*/

	public boolean isDeviceRegisted(String deviceId) throws IOException {
		//System.out.println("Connection String: "+connectionStringIoTHub);
		RegistryManager registryManager = getRegistryManager();//RegistryManager.createFromConnectionString(connectionStringIoTHub);
		Device device;
		try {
			device = registryManager.getDevice(deviceId);
			if (device.getDeviceId().equals(deviceId)) {
				return true;
			}
		} catch (JsonSyntaxException | IotHubException e) {
			// e.printStackTrace();
			return false;
		}

		return false;
	}

	@SuppressWarnings("unchecked")
	public JSONObject deviceRegistration(String deviceId) throws Exception {
		JSONObject jsonObject = new JSONObject();
		RegistryManager registryManager = getRegistryManager();//RegistryManager.createFromConnectionString(connectionStringIoTHub);
		Device device = Device.createFromId(deviceId, null, null);
		try {
			device = registryManager.addDevice(device);
		} catch (IotHubException iote) {
			// If the device already exists.
			try {
				device = registryManager.getDevice(deviceId);
			} catch (IotHubException iotf) {
				iotf.printStackTrace();
			}
		}
		jsonObject.put("DeviceId", device.getDeviceId());
		jsonObject.put("DeviceKey", device.getPrimaryKey());
		//System.out.println("Device Id: " + device.getDeviceId());
		//System.out.println("Device key: " + device.getPrimaryKey());
		return jsonObject;
	}

	@SuppressWarnings("unchecked")
	public JSONObject showAllDevices() throws IOException {
		JSONObject jsonObject = new JSONObject();
		ArrayList<String> deviceIDs = new ArrayList<String>();
		RegistryManager registryManager = getRegistryManager();//RegistryManager.createFromConnectionString(connectionStringIoTHub);
		try {
			ArrayList<Device> devices = registryManager.getDevices(Integer.MAX_VALUE);
			for (Device dev : devices) {
				deviceIDs.add(dev.getDeviceId().toString());
			}

		} catch (IotHubException e) {
			e.printStackTrace();
		}
		jsonObject.put("Devices", deviceIDs);
		return jsonObject;
	}

	@SuppressWarnings("unchecked")
	public JSONObject deleteDevice(String deviceId) throws IOException {

		JSONObject jsonObject = new JSONObject();
		RegistryManager registryManager = getRegistryManager();//RegistryManager.createFromConnectionString(connectionStringIoTHub);
		if (isDeviceRegisted(deviceId)) {
			try {
				registryManager.removeDevice(deviceId);
				jsonObject.put("Status", "Removed");
			} catch (IotHubException e) {
				jsonObject.put("Status", "Device Does not exist!");
			}
			
		} else
			jsonObject.put("Status", "Device Does not exist!");
			return jsonObject;
	}
	
	public RegistryManager getRegistryManager() throws IOException{
		return RegistryManager.createFromConnectionString(connectionStringIoTHub);
	}
}
